$(document).ready(async function(){
    
    // init webamp
    const Webamp = window.Webamp;
    const wa = new Webamp();
    await wa.renderWhenReady(document.getElementById('app'));
    
    // if webamp loads clear placeholder
    if ($("#webamp").length) {
        $("#app").css("background-image","none");
    }
    
    // autoload NL
    loadA('NouveauLife',0);
    
    // lazy load album covers
    $("label").click(function(){
        $(this).next().children("li").children("img").each(function(){
            $(this).attr({newTitleName: $(this).attr('src')}).attr("src",$(this).attr("wsrc"));
        });
    });
    
    // load clicked albums
    $("#dir li > a").click(function(){
        loadA($(this).attr('id'),1);
    });
    
    function loadA(id,s) {
        var md = "https://archive.org/metadata/" + id;
        $.getJSON(md, function(result){
            var fileobjs = $.grep(result.files, function (element, index) {
                return element.format === "VBR MP3";
            });
            // add each to queue
            $.each(fileobjs, function(i, file) {
                // new track object
                var t = new Object();
                t.metaData = new Object();
                t.metaData.artist = file.creator;
                t.metaData.title = file.title;
                t.url = "https://" + result.d1 + result.dir + "/" + encodeURIComponent(file.name);
                // add & play or just add
                if (i == 0 && s > 0) wa.setTracksToPlay([t]);
                else wa.appendTracks([t]);
            });
        });
    }
    
});