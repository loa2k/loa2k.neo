#!/usr/bin/env python3

"""
    LoA2K webpage update tool
    This is free and unencumbered software released into the public domain.
    License: https://unlicense.org
"""


"""-----------------------------------------------
GLOBAL PARAMTERS
-----------------------------------------------"""

# packages
import sqlite3

# quick 'n' dirty way to clear the console
clear = lambda: print('\n' * 250)

# cleaner, but less portable way
"""
clear = lambda: os.system(
    'cls' if os.name=='nt' else 'clear'
    )
"""

# files & folders
dir_base    = '../web/'
dir_button  = dir_base   + 'btns/'
lst_album   = dir_base   + '_dir.txt'
lst_button  = dir_button + '_btns.txt'
lst_missing = dir_base   + 'missing.txt'
htm_index   = dir_base   + 'index.html'

# user menu text
menu = '''
   _          __ ___  _  _
  | |   ___  /  |_  \| |/ /
  | |__| . |/ ' |/  /|  _  |
  |____|___|_/|_|____|_| |_|

  1  Reload album  list from {}
  2  Reload button list from {}
  3  Update index page albums
  4  Update index page buttons
  5  Generate missing album list into {}
  0  Exit
'''.format(
        lst_album,
        lst_button,
        lst_missing
    )

# messages after menu
msg_begin   = 'Type a number then press [Enter]'
msg_prompt  = 'What will it be?'
msg_bye     = 'Thank you for visiting LoA2K, ' +\
              'see you soon!\n'
err_typo    = 'Try again.'
err_reloada = 'Please reload album list first!'
err_reloadb = 'Please reload button list first!'
suc_albums  = 'Album list reloaded succesfully.'
suc_buttons = 'Button list reloaded succesfully.'
suc_indexa  = 'Index page albums updated ' +\
              'succesfully.'
suc_indexb  = 'Index page buttons updated ' +\
              'succesfully.'
suc_missing = 'Missing album list generated ' +\
              'succesfully.'

# internet archive urls
ia_img = 'https://archive.org/services/img/'
ia_dl1 = 'https://archive.org/compress/'
ia_dl2 = '/formats=JPEG,PNG,GIF,TXT,'
ia_flc = 'FLAC,24BIT%20FLAC'
ia_mp3 = 'VBR%20MP3'


"""-----------------------------------------------
DATABASE INIT
-----------------------------------------------"""

# create database in file/:memory:
conn = sqlite3.connect(':memory:')

# create column indexer so we can get a column by
# its name e.g. ['url'] instead of number e.g. [3]
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
# assign indexer to database
conn.row_factory = dict_factory

# create database cursor
curs = conn.cursor()

# create album table
# allowed datatypes: null integer real text blob
curs.execute(
    '''
    CREATE TABLE albums (
        artist text,
        status text,
        title  text,
        url    text
    )
    '''
)

# create button database (1D list)
# instead of initializing an empty list,
# and having to define a type, I just do this:
button_db = ['put', 'buttons', 'here :)']
button_db.clear()


"""-----------------------------------------------
HELPER FUNCTIONS
-----------------------------------------------"""

# format artist
def form_artist(record, form):
    # txt
    if form == 1:
        return record['artist'].strip() + '\n'
    # html
    elif form == 2:
        # row number (artist id)
        a_id = record['rowid']
        # artist name
        artist = record['artist'].strip()
        # html block
        return (
'''\
<input class="check" id="check{0}" \
type="checkbox"></input>\
<label for="check{0}">{1}</label>
<ul>
'''.format(
            a_id,                      # 0
            artist.replace('<','&lt;'
                 ).replace('>','&gt;') # 1
            )
        )
    # fallback
    return None


# format album
def form_album(record, form):
    # txt
    if form == 1:
        # write current album url (if it exists)
        if record['url'].strip():
            return (
                record['status'].strip() + ' ' +
                record['title' ].strip() + ';' +
                record['url'   ].strip()
            )
        # otherwise omit it completely
        else:
            return (
                record['status'].strip() + ' ' +
                record['title' ].strip()
            )
    # html
    elif form == 2:
        # album title & url
        title = record['title'].strip()
        url   = record['url'].strip()
        # download link
        ia_dl = ia_dl1 + url + ia_dl2
        # html block
        return (
'''\
<li><img wsrc="{0}{1}"><a id="{1}">{2}</a><br>
<span><a href="{3}">FLAC</a>
<a href="{4}">MP3</a></span></li>\
'''.format(
            ia_img,                    # 0
            url,                       # 1
            title.replace('<','&lt;'   # 2
                ).replace('>','&gt;'),
            ia_dl + ia_flc,            # 3
            ia_dl + ia_mp3             # 4
            )
        )
    # fallback
    return None


# format section after artist
def form_ending(form):
    # txt
    if form == 1:
        return '\n'
    # html
    elif form == 2:
        return '</ul>\n'
    # fallback
    return None


# write a set of albums to a file
# in txt format (1) or html format (2)
def write_catalog(filterset, outfile, form):
    # previous artist (initially blank)
    old_artist = ''
    # write every record in database
    for record in filterset:
        # get current artist
        new_artist = record['artist']
        # is current different from previous?
        if old_artist != new_artist:
            # close current artist
            # unless it's the first artist
            if old_artist != '':
                outfile.write(
                    form_ending(form)
                )
            # write current artist name
            outfile.write(
                form_artist(record, form)
            )
        # write current album name
        outfile.write(
            form_album(record, form)
        )
        # line break for next album
        outfile.write('\n')
        # set previous artist
        old_artist = new_artist


# write buttons to a file
def write_buttons(outfile):
    # target global button database
    global button_db
    # get relative button directory for img src
    dir_button_ = dir_button[len(dir_base):]
    # iterate over button database
    for btn in button_db:
        cur_btn = btn.split(';')
        # check if current button has a file
        if len(cur_btn[0].split('.')) < 2 :
            continue
        # write html code for button
        outfile.write(
'''\
<a
href="{0}" target="_blank">\
<img src="{1}" alt="{2}" title="{2}"></a>\
'''.format(
            cur_btn[2].strip(),       # 0
            dir_button_ + cur_btn[0], # 1
            cur_btn[1]                # 2
            )
        )


"""-----------------------------------------------
MASTER FUNCTIONS
-----------------------------------------------"""

# reload album list (2-pass)
def reload_albums():
    # clear table
    curs.execute('DELETE FROM albums')
    # first pass
    # read album list file into database
    al_file = open(lst_album, 'r')
    # iterate over lines
    while True:
        # read 1 line
        line = al_file.readline()
        # ignore empty lines & stop at EOF
        if not line:
            break
        if line.isspace():
            continue
        # begin artist
        artist = line.strip()     # remove newline
        line = al_file.readline() # next line
        # begin albums
        while line and not line.isspace():
            # - / # / $
            status = line[0:1]
            # [ALBUM] / [ALBUM,URL]
            album_url = line[2:].split(';')
            # ALBUM
            title = album_url[0]
            # URL (blank)
            url   = ''
            # URL (if exists)
            if len(album_url) == 2:
                url = album_url[1]
            # add album to db
            curs.execute(
                '''
                INSERT INTO albums
                VALUES (?,?,?,?)
                ''' ,
                (artist, status, title, url)
            )
            # next line
            line = al_file.readline()
    # finalizing pass 1
    al_file.close() # done reading
    conn.commit()   # push all actions
    curs.execute(   # sort database
        '''
        SELECT * FROM albums
        ORDER BY
        artist COLLATE NOCASE ASC,
        title COLLATE NOCASE ASC
        '''
    )
    # store sorted albums in a set
    filterset = curs.fetchall()
    # second pass
    # dump sorted database into album list file
    al_file = open(lst_album, 'w')
    write_catalog(filterset, al_file, 1)
    al_file.close() # done writing
    return display_menu(suc_albums)


# reload button list
def reload_buttons():
    # target global button database
    global button_db
    # read button list from file into database
    bt_file = open(lst_button, 'r')
    button_db.clear()
    button_db = bt_file.readlines()
    bt_file.close() # done reading
    # dump database into button list file
    bt_file = open(lst_button, 'w')
    button_db.sort()
    ### TODO remove empty lines
    bt_file.writelines(button_db)
    bt_file.close() # done writing
    return display_menu(suc_buttons)


# update index page albums
def index_albums():
    # filter database
    curs.execute(
        '''
        SELECT rowid, * FROM albums
        WHERE status == '#'
        ORDER BY
        artist COLLATE NOCASE ASC,
        title COLLATE NOCASE ASC
        '''
    )
    filterset = curs.fetchall()
    # notify user of empty database
    if len(filterset) == 0:
        return display_menu(err_reloada)
    # read the entire file into a string
    ix_file = open(htm_index, 'r')
    allofit = ix_file.read()
    # isolate code before & after album list div
    bef_div = allofit.split('<div id="dir">')
    aft_div = allofit.split('</div><!-- dir -->')
    ix_file.close() # done reading
    # start writing
    ix_file = open(htm_index, 'w')
    # write code before album list div
    ix_file.write(
        bef_div[0] +
        '<div id="dir">\n'
    )
    # write album list div contents
    write_catalog(filterset, ix_file, 2)
    # write code after album list div
    ix_file.write(
'''\
</ul>
</div><!-- dir -->\
'''
        + aft_div[1]
    )
    # finalizing
    ix_file.close() # done writing
    return display_menu(suc_indexa)


# update index page buttons
def index_buttons():
    # target global button database
    global button_db
    # notify user of empty list
    if len(button_db) == 0:
        return display_menu(err_reloadb)
    # read the entire file into a string
    ix_file = open(htm_index, 'r')
    allofit = ix_file.read()
    # isolate code before & after button list div
    bef_div = allofit.split('<div id="btns">')
    aft_div = allofit.split('</div><!-- btns -->')
    ix_file.close() # done reading
    # start writing
    ix_file = open(htm_index, 'w')
    # write code before button list div
    ix_file.write(
        bef_div[0] +
        '<div id="btns">'
    )
    ### TODO write button list
    write_buttons(ix_file)
    # write code after button list div
    ix_file.write(
        '\n' +
        '</div><!-- btns -->' +
        aft_div[1]
    )
    # finalizing
    ix_file.close() # done writing
    return display_menu(suc_indexb)


# generate missing albums list
def gen_missing():
    # filter database
    curs.execute(
        '''
        SELECT * FROM albums
        WHERE status == '-'
        ORDER BY
        artist COLLATE NOCASE ASC,
        title COLLATE NOCASE ASC
        '''
    )
    filterset = curs.fetchall()
    # notify user of empty database
    if len(filterset) == 0:
        return display_menu(err_reloada)
    # open missing list file
    ms_file = open(lst_missing, 'w')
    write_catalog(filterset, ms_file, 1)
    # finalizing
    ms_file.close() # done writing
    return display_menu(suc_missing)


# terminate program
def leave():
    conn.close() # close database
    print(msg_bye)
    # sys.exit()
    return 0


"""-----------------------------------------------
START POINT
-----------------------------------------------"""

# print user menu & message then take input
def display_menu(msg):
    clear()
    print(menu)
    print(msg)
    # take user's choice of action as input
    # str() is just a precaution
    print(msg_prompt, end=' ')
    choice = str(input())
    # execute action if valid
    if choice in select:
        select[choice]()
    # otherwise let user try again
    else:
        display_menu(err_typo)

# action dictionary
select = {
    '1' : reload_albums,
    '2' : reload_buttons,
    '3' : index_albums,
    '4' : index_buttons,
    '5' : gen_missing,
    '0' : leave
}

# main function
if __name__ == "__main__":
    display_menu(msg_begin)
