# LoA2K

![](gfx/header/thumb_wide.jpg)

The Library Of Alexandria 2000 \[LOA2K\] is an ongoing archival effort for preserving deleted and inaccessible vaporwave music, so that it may be available to stream and download for generations to come.

This repo holds the software that generates and updates webpages for the [LoA2K website](https://loa2k.neocities.org/).

## Installation

Install [Python 3](https://www.python.org/downloads/) on your computer (Windows users will need to add it to PATH during setup) then download this repository anywhere you like.

## Usage

1. Open a terminal/command window inside the `py` folder.
1. Type `python3 loa.py` and press \[Enter\].
1. Type the number for the operation you want then press \[Enter\].

![](gfx/screenshot.png)

## License

[Unlicense](LICENSE) (public domain)
